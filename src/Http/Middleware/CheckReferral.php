<?php

/*
 * This file is part of blok/laravel-referral package.
 *
 * (c) blok <zhengchaopu@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Blok\Referral\Http\Middleware;

use Closure;
use Cookie;

class CheckReferral
{
    public function handle($request, Closure $next)
    {
        if ($request->hasCookie('referral')) {
            return $next($request);
        }

        if (($ref = $request->query('ref')) && app(config('referral.user_model', 'App\User'))->referralExists($ref)) {
            Cookie::queue(Cookie::forever('referral', $ref));
        }

        return $next($request);
    }
}
